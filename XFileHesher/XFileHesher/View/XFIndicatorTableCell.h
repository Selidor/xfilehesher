//
//  XFIndicatorTableCell.h
//  MPawsX-Files
//
//  Created by Artem Shvets on 10.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface XFIndicatorTableCell : NSTableCellView
- (void)setProgress:(NSUInteger)progress;
@end

NS_ASSUME_NONNULL_END
