//
//  FileHesher_XPCService.m
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "FileHesherXPCService.h"
#import <CommonCrypto/CommonCrypto.h>

static const unsigned int bufferSize = 1024;//1kb;

@interface FileHesherXPCService ()

@property (nonatomic, strong) NSXPCConnection *connection;
@property (nonatomic, strong) id<FileProgressServiceProtocol> owner;
@property (nonatomic, strong) dispatch_queue_t heshCalcQueue;
@property (nonatomic, strong) NSMutableDictionary *progressForPath;
@property (nonatomic, strong) NSMutableDictionary *heshesForPath;
@property (nonatomic, strong) NSMutableArray *filePathsInProcess;
@property (nonatomic, strong) dispatch_semaphore_t concurrencyLimitingSemaphore;

@end

@implementation FileHesherXPCService

- (instancetype)initWithConnection:(NSXPCConnection *)connection {
    
    self = [super init];
    if(self){
        
        _concurrencyLimitingSemaphore = dispatch_semaphore_create(100);
        _connection = connection;
        _filePathsInProcess = [NSMutableArray new];
        _progressForPath = [NSMutableDictionary new];
        _heshesForPath = [NSMutableDictionary new];
        
        _heshCalcQueue = dispatch_queue_create("XF.FileHesherXPCService.calculateHesh", DISPATCH_QUEUE_CONCURRENT);
        _owner = [connection remoteObjectProxyWithErrorHandler:^(NSError * _Nonnull error) {
            NSLog(@"ERROR%@", error.description);
        }];
    }
    return self;
}

- (void)calculateHashsOfFiles:(NSArray<NSString *> *)filePaths {
    
    __weak typeof(self) weakSelf = self;
    
    for (NSString *path in filePaths) {
        
        if([weakSelf.filePathsInProcess containsObject:path])
            continue;
        
        @synchronized (self) {
            [weakSelf.filePathsInProcess addObject:path];
        }
        
        dispatch_async(_heshCalcQueue, ^{
            [weakSelf fileMD5forPath:path progress:^(NSUInteger progress, NSString *path) {
                
                if(weakSelf.owner){
                    
                    @synchronized (self) {
                        
                        [weakSelf.progressForPath setObject:@(progress) forKey:path];
                        [weakSelf.owner updateProgress:weakSelf.progressForPath];
                    }
                }
            } completion:^(NSString *hash,NSString *path, NSError *error) {
                
                @synchronized (self) {
                    
                    [weakSelf.filePathsInProcess removeObject:path];
                    [weakSelf.progressForPath removeObjectForKey:path];
                    [weakSelf.heshesForPath removeObjectForKey:path];
                    
                    if(weakSelf.owner){
                        [weakSelf.connection.remoteObjectProxy didCalculateHash:hash atPath:path errorMessage:error.localizedDescription];
                    }
                }
            }];
        });
    }
}

- (void)fileMD5forPath:(NSString *)path
              progress:(void (^)(NSUInteger progress, NSString *path))progressHandler
            completion:(void (^)(NSString *hash, NSString *path, NSError *error))completionHandler{
    

    dispatch_semaphore_wait(self.concurrencyLimitingSemaphore, DISPATCH_TIME_FOREVER);
    
    NSError *error = nil;
    NSURL *url = [NSURL URLWithString:[path stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingFromURL:url error:&error];
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
    
    unsigned long long fileSize = [attributes fileSize];
    
    if(handle == nil){
        completionHandler(@"error", path, error);
        return;
    }
    
    CC_MD5_CTX md5;
    
    CC_MD5_Init(&md5);
    
    int steps = ceil((float)fileSize / (float)bufferSize);
    static int percent = 100.0;
    
    for (long i = 0; i <= steps; ++i) {
        
        @autoreleasepool {
            NSData* fileData = [handle readDataOfLength: bufferSize];
            CC_MD5_Update(&md5, [fileData bytes], (unsigned int)[fileData length]);
            
            NSUInteger progress = (NSUInteger)((float)i / (float)steps * percent);
            if(progress > 0){
                progressHandler(progress, path);
            }
        }
        
    }
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5);
    NSString* s = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                   digest[0], digest[1],
                   digest[2], digest[3],
                   digest[4], digest[5],
                   digest[6], digest[7],
                   digest[8], digest[9],
                   digest[10], digest[11],
                   digest[12], digest[13],
                   digest[14], digest[15]];
    
    completionHandler(s, path, error);
    
    dispatch_semaphore_signal(self.concurrencyLimitingSemaphore);
}



@end
