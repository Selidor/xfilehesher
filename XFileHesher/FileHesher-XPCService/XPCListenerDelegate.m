//
//  XPCListenerDelegate.m
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "XPCListenerDelegate.h"
#import "FileHesherXPCService.h"

@interface XPCListenerDelegate ()
@property (nonatomic, strong) FileHesherXPCService *service;
@end
@implementation XPCListenerDelegate

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection {
    
    newConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileHesherXPCServiceProtocol)];
    newConnection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileProgressServiceProtocol)];
    
    if(!_service){
        _service = [[FileHesherXPCService alloc] initWithConnection:newConnection];
    }
    
    newConnection.exportedObject = _service;
    newConnection.invalidationHandler = ^{
        NSLog(@"Invalid XPC connection");
    };
    newConnection.interruptionHandler = ^{
        NSLog(@"Interrupt XPC connection");
    };
    
    [newConnection resume];
    
    return YES;
}

@end
