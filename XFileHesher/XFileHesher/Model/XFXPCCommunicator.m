//
//  XFxpcAgent.m
//  XFileHesher
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "XFXPCCommunicator.h"
#import "FileHesherXPCServiceProtocol.h"

#define XF_FILE_HESHER_SERVICE_NAME @"XF.FileHesher-XPCService"

@interface XFXPCCommunicator ()<FileProgressServiceProtocol>
@property (nonatomic, strong) NSMutableArray <XFFile *>*files;
@property (nonatomic, strong) NSXPCConnection *connection;
@property (nonatomic, strong) id<FileHesherXPCServiceProtocol> service;
@end

@implementation XFXPCCommunicator

- (instancetype)init{
    
    self = [super init];
    if(self){
        _files = [NSMutableArray new];
    }
    return self;
}

- (void)connectToXPCService{
    
    _connection = [[NSXPCConnection alloc] initWithServiceName:XF_FILE_HESHER_SERVICE_NAME];
    _connection.exportedObject = self;
    _connection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileProgressServiceProtocol)];
    _connection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileHesherXPCServiceProtocol)];
    _connection.invalidationHandler = ^{
        NSLog(@"Invalid XPC connection");
    };
    _connection.interruptionHandler = ^{
        NSLog(@"Interrupt XPC connection");
    };
    
    [_connection resume];
    
    _service = [_connection remoteObjectProxyWithErrorHandler:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.description);
    }];
}

- (void)calculateHeshForSelectedFiles:(NSIndexSet *)indexOfFiles {
    
    NSArray* selected = [_files objectsAtIndexes:indexOfFiles];
    NSMutableArray <NSString *>*paths = [NSMutableArray new];
    
    for (XFFile *file in selected) {
        file.progress = 0;
        file.calculatedHesh = @"-";
        [paths addObject:file.url.relativePath];
    }
    
    [self.service calculateHashsOfFiles:paths];
}

- (void)addFilesForURLs:(NSArray<NSURL *> *)urls {
    
    for (NSURL *url in urls) {
        XFFile *file = [[XFFile alloc] initWithURL:url];
        if(![_files containsObject:file]){
            [_files addObject:file];
        }
    }
}

- (void)removeSelectedFiles:(NSIndexSet *)indexOfFiles {
    
    NSArray *remData = [_files objectsAtIndexes:indexOfFiles];
    for (XFFile *file in remData) {
        [file unsubscribeAll];
    }
    [_files removeObjectsInArray:remData];
}

#pragma mark - Service Handler Protocol

- (void)didCalculateHash:(NSString *)hash atPath:(NSString *)path errorMessage:(NSString *)errorMessage {
    
    NSArray *arr = [self.files copy];
    
    for (XFFile *file in arr) {
        if([file.url.relativePath isEqualToString:path]){
            file.calculatedHesh = hash;
        }
    }
}

- (void)updateProgress:(NSDictionary *)progress {
    
    NSArray *arr = [self.files copy];
    for (NSString *path in progress.allKeys) {
        NSUInteger prog = [progress[path] integerValue];
        for (XFFile *file in arr) {
            if([file.url.relativePath isEqualToString:path] && file.progress < prog){
                file.progress = prog;
            }
        }
    }
}

@end
