//
//  AppDelegate.m
//  XFileHesher
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "AppDelegate.h"
#import "XFXPCCommunicator.h"
#import "XFTableViewController.h"
#import "XFAppCoordinator.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    [[XFAppCoordinator sharedInstance] start];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
