//
//  XFxpcAgent.h
//  XFileHesher
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XFFile.h"

NS_ASSUME_NONNULL_BEGIN

@interface XFXPCCommunicator : NSObject
@property (nonatomic, readonly) NSArray <XFFile *>*files;

- (void)connectToXPCService;
- (void)calculateHeshForSelectedFiles:(NSIndexSet*)indexOfFiles;
- (void)addFilesForURLs:(NSArray<NSURL *>*)urls;
- (void)removeSelectedFiles:(NSIndexSet*)indexOfFiles;

@end

NS_ASSUME_NONNULL_END
