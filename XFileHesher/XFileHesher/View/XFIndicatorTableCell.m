//
//  XFIndicatorTableCell.m
//  MPawsX-Files
//
//  Created by Artem Shvets on 10.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "XFIndicatorTableCell.h"

@interface XFIndicatorTableCell ()
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;
@property (weak) IBOutlet NSTextField *progressLabel;
@end
@implementation XFIndicatorTableCell

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)setProgress:(NSUInteger)progress{
    
    self.progressIndicator.doubleValue = progress;
    self.progressLabel.stringValue = [NSString stringWithFormat:@"%lu", progress];
}

@end
