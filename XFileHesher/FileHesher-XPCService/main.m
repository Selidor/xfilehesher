//
//  main.m
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileHesherXPCService.h"
#import "XPCListenerDelegate.h"

int main(int argc, const char *argv[])
{
    XPCListenerDelegate *delegate = [XPCListenerDelegate new];
    NSXPCListener *listener = [NSXPCListener serviceListener];
    listener.delegate = delegate;
    [listener resume];
    return 0;
}
