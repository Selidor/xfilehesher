//
//  XFTableViewController.h
//  XFileHesher
//
//  Created by Artem Shvets on 17.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "XFXPCCommunicator.h"

NS_ASSUME_NONNULL_BEGIN

@interface XFTableViewController : NSViewController
@end

NS_ASSUME_NONNULL_END
