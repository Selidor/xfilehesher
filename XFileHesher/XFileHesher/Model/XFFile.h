//
//  XFFileStruct.h
//  MPawsX-Files
//
//  Created by Artem Shvets on 09.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ProgressHandlerBlock)(NSUInteger progress);
typedef void (^CalculatedHeshBlock)(NSString *hesh);

@interface XFFile : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic) NSUInteger progress;// from 0 to 100
@property (nonatomic) NSString *calculatedHesh;

- (instancetype)initWithURL:(NSURL*)url;
- (void)unsubscribeAll;
- (void)handleProgress:(ProgressHandlerBlock)progressHandler;
- (void)handleHesh:(CalculatedHeshBlock)progressHandler;

@end
