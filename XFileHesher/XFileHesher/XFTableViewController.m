//
//  XFTableViewController.m
//  XFileHesher
//
//  Created by Artem Shvets on 17.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "XFTableViewController.h"
#import "XFIndicatorTableCell.h"
#import "XFAppCoordinator.h"

#define FILE_NAME_COLUMN_ID @"FileName"
#define FILE_PROGRESS_COLUMN_ID @"FileProgress"
#define FILE_CALCULATED_HESH_COLUMN_ID @"CalculatedHash"
#define FILE_EMPTY_HESH @"-"

@interface XFTableViewController ()<NSTableViewDelegate, NSTableViewDataSource>

@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSButton *calculatedHashButton;
@property (weak) IBOutlet NSButton *deleteButton;
@property (weak) IBOutlet NSButton *addFilesButton;
@property (nonatomic, strong) XFXPCCommunicator *xpcCommunicator;

@end

@implementation XFTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.xpcCommunicator = [XFAppCoordinator sharedInstance].xpcCommunicator;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (IBAction)addFilesAction:(id)sender {
    
    [self chooseFiles];
}

- (IBAction)deleteSelectedFileAction:(id)sender {
    
    [self.xpcCommunicator removeSelectedFiles:self.tableView.selectedRowIndexes];
    [self.tableView reloadData];
}
- (IBAction)calculateSelectedFilesAction:(id)sender {
    
    [self.xpcCommunicator calculateHeshForSelectedFiles:self.tableView.selectedRowIndexes];
}

- (void)chooseFiles{
    
    __weak typeof(self) weakSelf = self;
    
    [[XFAppCoordinator sharedInstance] selectFiles:^(NSArray<NSURL *> * _Nonnull urls) {
        [weakSelf.xpcCommunicator addFilesForURLs:urls];
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - NSTableView Delegate & DataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView{
    
    return self.xpcCommunicator.files.count;
}

- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row{
    
    XFFile *fileStruct = self.self.xpcCommunicator.files[row];
    NSTableCellView *cell = [tableView makeViewWithIdentifier:tableColumn.identifier owner:nil];
    
    if ([tableColumn.identifier isEqualToString:FILE_NAME_COLUMN_ID]){
        
        [self configureNameCell:cell file:fileStruct];
        
    }else if ([tableColumn.identifier isEqualToString:FILE_PROGRESS_COLUMN_ID]){
        
        [self configureProgressCell:cell file:fileStruct];
        
    }else if ([tableColumn.identifier isEqualToString:FILE_CALCULATED_HESH_COLUMN_ID]){
        
        [self configureHashCell:cell file:fileStruct];
    }
    
    return cell;
}

#pragma mark - Cell Configure


- (void)configureNameCell:(NSTableCellView *)cell file:(XFFile *)file{
    cell.textField.stringValue = file.name;
}

- (void)configureProgressCell:(NSTableCellView *)cell file:(XFFile *)file{
    
    if([cell isKindOfClass:[XFIndicatorTableCell class]]){
        XFIndicatorTableCell *progressCell = (XFIndicatorTableCell *)cell;
        [progressCell setProgress:file.progress];
        
        __weak typeof(progressCell) weakCell = progressCell;
        
        [file handleProgress:^(NSUInteger progress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakCell setProgress:file.progress];
            });
        }];
    }
}

- (void)configureHashCell:(NSTableCellView *)cell file:(XFFile *)file{
    
    cell.textField.stringValue = file.calculatedHesh ? file.calculatedHesh : FILE_EMPTY_HESH;
    
    __weak typeof(cell) weakCell = cell;
    
    [file handleHesh:^(NSString * _Nonnull hesh) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakCell.textField.stringValue = hesh;
        });
    }];
}

@end
