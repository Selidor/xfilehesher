//
//  FileHesher_XPCServiceProtocol.h
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FileHesherXPCServiceProtocol

- (instancetype)initWithConnection:(NSXPCConnection *)connection;
- (void)calculateHashsOfFiles:(NSArray<NSString *>* )filePaths;

@end

@protocol FileProgressServiceProtocol

- (void)updateProgress:(NSDictionary *)progress;
- (void)didCalculateHash:(NSString *)hash atPath:(NSString *)path errorMessage:(NSString *)errorMessage;

@end
