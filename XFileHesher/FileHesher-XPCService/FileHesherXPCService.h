//
//  FileHesher_XPCService.h
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileHesherXPCServiceProtocol.h"

@interface FileHesherXPCService : NSObject <FileHesherXPCServiceProtocol>

@end
