//
//  XFAppCoordinator.m
//  XFileHesher
//
//  Created by Artem Shvets on 08.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "XFAppCoordinator.h"
#import "XFTableViewController.h"

@implementation XFAppCoordinator

+ (instancetype)sharedInstance{
    
    static XFAppCoordinator *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[XFAppCoordinator alloc] init];
    });
    return sharedInstance;
}

- (void)start{
    
    self.xpcCommunicator = [XFXPCCommunicator new];
    [self.xpcCommunicator connectToXPCService];
    
    [self displayMainWindow];
}

- (void)selectFiles:(SelectFilesHandler)selectFilesBlock{
    
    NSOpenPanel *openPanel = nil;
    @try
    {
        openPanel = [NSOpenPanel openPanel];
        
        openPanel.canChooseDirectories = NO;
        openPanel.canCreateDirectories = NO;
        openPanel.canChooseFiles = YES;
        openPanel.allowsMultipleSelection = YES;
        [openPanel setTitle:@"Select Files"];
    }
    @catch (NSException *exception)
    {
        openPanel = nil;
        
        NSString *theMessage = [NSString stringWithFormat:@"Failed to create Open Panel due to exception: %@", [exception description]];
        NSString *theTitle = @"An Exeption Occured";
        NSAlert *alert = [NSAlert new];
        [alert setMessageText:theTitle];
        [alert setInformativeText:theMessage];
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    }
    
    [openPanel beginSheetModalForWindow:[NSApp mainWindow] completionHandler:^(NSInteger aReturnCode){
        if (aReturnCode == NSModalResponseOK){
            NSArray* urls = openPanel.URLs;
            selectFilesBlock(urls);
        }
    }];
}


- (void)displayMainWindow{
    
    NSStoryboard *mainStoryboard = [NSStoryboard storyboardWithName:@"Main" bundle:nil];
    NSWindowController *initialController = [mainStoryboard instantiateControllerWithIdentifier:@"mainWindowController"];
    [initialController showWindow:self];
    [initialController.window makeKeyAndOrderFront:self];
}

@end
