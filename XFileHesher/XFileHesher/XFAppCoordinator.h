//
//  XFAppCoordinator.h
//  XFileHesher
//
//  Created by Artem Shvets on 08.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XFXPCCommunicator.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^SelectFilesHandler)(NSArray <NSURL *>* urls);

@interface XFAppCoordinator : NSObject
@property (nonatomic, strong) XFXPCCommunicator *xpcCommunicator;

+ (instancetype)sharedInstance;
- (void)selectFiles:(SelectFilesHandler)selectFilesBlock;
- (void)start;

@end

NS_ASSUME_NONNULL_END
