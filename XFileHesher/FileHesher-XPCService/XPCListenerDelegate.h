//
//  XPCListenerDelegate.h
//  FileHesher-XPCService
//
//  Created by Artem Shvets on 16.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XPCListenerDelegate : NSObject <NSXPCListenerDelegate>

@end

NS_ASSUME_NONNULL_END
