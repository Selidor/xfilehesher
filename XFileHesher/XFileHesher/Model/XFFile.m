//
//  XFFile.m
//  MPawsX-Files
//
//  Created by Artem Shvets on 09.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "XFFile.h"


@interface XFFile ()
@property (nonatomic) ProgressHandlerBlock progressHandler;
@property (nonatomic) CalculatedHeshBlock heshHandler;
@end

@implementation XFFile

- (instancetype)initWithURL:(NSURL *)url{
    
    self = [super init];
    if(self){
        _url = url;
    }
    return self;
}

- (void)dealloc{
    NSLog(@"DEALLOC_FILE");
}

- (BOOL)isEqual:(id)object{
    
    return (object == self || ([object isKindOfClass:[XFFile class]] && [[object url] isEqual:self.url]));
}

- (NSString *)name{
    
    if(self.url){
        return self.url.pathComponents.lastObject;
    }
    return nil;
}

- (void)unsubscribeAll{
    
    _progressHandler = nil;
    _heshHandler = nil;
}
- (void)setProgress:(NSUInteger)progress{
    
    if(progress != _progress){
        _progress = progress;
        
        if(self.progressHandler){
            self.progressHandler(progress);
        }
    }
}

- (void)setCalculatedHesh:(NSString *)calculatedHesh{
    
    if(calculatedHesh != _calculatedHesh){
        
        _calculatedHesh = calculatedHesh;
        
        if(self.heshHandler){
            self.heshHandler(calculatedHesh);
        }
    }
}

- (void)handleProgress:(ProgressHandlerBlock)progressHandler{
    
    if(_progressHandler != progressHandler){
        self.progressHandler = progressHandler;
    }
    
}
- (void)handleHesh:(CalculatedHeshBlock)heshHandler{
    
    if(_heshHandler != heshHandler){
        self.heshHandler = heshHandler;
    }
}

@end
